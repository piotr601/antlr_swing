grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (stat | group)+ EOF!;

stat
    : expr NL -> expr
    | VAR ID PODST expr NL -> ^(VAR ID) ^(PODST ID expr)
    | VAR ID NL -> ^(VAR ID)
    | ID PODST expr NL -> ^(PODST ID expr)
    | if_statement NL -> if_statement
    | NL ->
    ;

FUN :;
TYP: 'int' | 'float';
COMMA : ',';

fundecl :
    TYP ID LP^ params? RP!    
    ;

params : 
        TYP ID (COMMA TYP ID)*
;

LCB : '{';
RCB : '}';

group:
      LCB^ (stat | group)* RCB!
      ;

expr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;

multExpr
    : atom
      ( MUL^ atom
      | DIV^ atom
      )*
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    ;

if_statement 
    : IF^ expr THEN! (expr) (ELSE! expr)?
    ;
    
IF : 'if' ;
ELSE : 'else' ;
THEN : 'then' ;

VAR :'var';

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\n' 
   ;

WS : (' ' | '\t' | '\r')+ {$channel = HIDDEN;} ;

LP
	:	'('
	;

RP
	:	')'
	;

PODST
	:	'='
	;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;

MUL
	:	'*'
	;

DIV
	:	'/'
	;
